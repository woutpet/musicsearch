// var my_client_id = '446c35b95b01414b8f5d01e3589ef4a9'; // Your client id
// var my_secret = '7ffc5fd7795c4547a649f192eb2fd0bc'; // Your secret
// var redirect_uri = 'http://localhost:8888/index.html'; // Your redirect uri

var resultList;

var defaultText;

var favoritesNode;

var favorites = [];

var storageFavorites;

init();

function init(){

	//window.localStorage.clear();

	// for (var key in localStorage) {
	//   console.log(key + ':' + localStorage[key]);
	// }

	defaultText = document.createElement("h2");
	defaultText.innerHTML = "Please give some input to see results";

	var searchbox = document.getElementById('searchbox');
	searchbox.addEventListener("keydown", search);

	resultList = document.getElementById('resultList');

	favoritesNode = document.getElementById('favorites');

	checkFavorites();

}

function checkFavorites(){

	if (window.localStorage) {

		favoritesNode.innerHTML = "";

		for (var key in localStorage) {

			//var favoriteDiv = document.createElement('div');

			var dataArray = localStorage[key].split(";;");

			//console.log(dataArray);

			var favoriteLi = document.createElement('li');
			favoriteLi.setAttribute("id", dataArray[3]);

			var favoriteLink = document.createElement('a');
			favoriteLink.setAttribute('href', dataArray[2]);

			var favoriteImg = document.createElement("img");
			favoriteImg.setAttribute("src", dataArray[1]);
			favoriteLink.appendChild(favoriteImg);
			//favoriteDiv.appendChild(favoriteLink);

			favoriteLink.setAttribute("class", "favoriteItem");

			favoriteLi.appendChild(favoriteLink);

			var deleteButton = document.createElement("a");
			deleteButton.innerHTML = "Delete";
			deleteButton.href = "";
			deleteButton.style.display = "block";
			deleteButton.addEventListener("click", deleteFav);

			favoriteLi.appendChild(deleteButton);

			favoritesNode.appendChild(favoriteLi);

			document.getElementById("favTitle").style.display = "initial";
			document.getElementById("favorites").style.display = "block";

			// window.localStorage.getItem(favorites)[i]
		};
	}else{

		favorites = [];

	}

	if (favoritesNode.innerHTML == "") {
		document.getElementById("favTitle").style.display = "none";
		document.getElementById("favorites").style.display = "none";
	};

}


function search(e){

	// $(".resultsSection").fadeOut(100);

	// resultList.innerHTML = defaultText;

	resultList.innerHTML = "";

	if (e.keyCode == 13) {
		e.preventDefault();

		var query = e.currentTarget.value;

		if (query != "") {
			$.ajax({

				type: "GET",
				url: "https://api.spotify.com/v1/search?limit=3&type=album&q=" + query,
				success: function(data){

					if (data.albums.items.length > 0) {

						var spotifyDiv = document.createElement("div");
						var spotifyH1 = document.createElement("h1");
						spotifyH1.innerHTML = "Spotify"
						spotifyDiv.appendChild(spotifyH1);
						

						for (var i = 2; i >= 0; i--) {

							var div = document.createElement("div");
							div.setAttribute("class", "result");

							var header = document.createElement("header");
							div.appendChild(header);


							var artImg = document.createElement("img");
							artImg.setAttribute("class", "albumImage");
							artImg.width = 150;
							artImg.height = 150;
							artImg.src = data.albums.items[i].images[1].url;
							header.appendChild(artImg);

							var ul = document.createElement("ul");

							var albumName = document.createElement("li");
							albumName.innerHTML = data.albums.items[i].name;
							albumName.setAttribute("class", "albumName");
							ul.appendChild(albumName);

							var linkLI = document.createElement("li");
							var linkNode = document.createElement("a");
							linkNode.setAttribute("href", data.albums.items[i].external_urls.spotify);
							linkNode.setAttribute("class", "albumLink");
							linkNode.innerHTML = "<img src='assets/play.svg' width='50' height='50'>"
							linkLI.appendChild(linkNode);
							ul.appendChild(linkLI);

							var addFav = document.createElement("li");
							addFav.innerHTML= "<a href=''>Add to favorites</a>"
							addFav.setAttribute("class", "addToFav");
							addFav.addEventListener("click", addToFav);
							div.appendChild(ul);
							div.appendChild(addFav);

							spotifyDiv.appendChild(div);

						}
					}else{

							var h2 = document.createElement("h2");
							h2.innerHTML = "No results found...";

							console.log

							spotifyDiv.appendChild(h2);

					}

					$(".resultsSection").append(spotifyDiv);

				}

			});
		};

		$.ajax({
			type: "GET",
			url: "https://itunes.apple.com/search?limit=3&entity=album&term=" + query,
			crossDomain: true,
			dataType: 'jsonp',
			success: function(data){

					console.log(data);

					if (data.results.length > 0) {

						var itunesDiv = document.createElement("div");
						var itunesH1 = document.createElement("h1");
						itunesH1.innerHTML = "iTunes"
						itunesDiv.appendChild(itunesH1);

						for (var i = 2; i >= 0; i--) {

							var div = document.createElement("div");
							div.setAttribute("class", "result");

							var header = document.createElement("header");
							div.appendChild(header);


							var artImg = document.createElement("img");
							artImg.setAttribute("class", "albumImage");
							artImg.width = 150;
							artImg.height = 150;
							artImg.src = data.results[i].artworkUrl100;
							header.appendChild(artImg);

							var ul = document.createElement("ul");
							div.appendChild(ul);

							var albumName = document.createElement("li");
							albumName.innerHTML = data.results[i].collectionName;
							albumName.setAttribute("class", "albumName");
							ul.appendChild(albumName);

							var price = document.createElement("li");
							price.innerHTML = "Price: " + data.results[i].collectionPrice;
							ul.appendChild(price);

							var link = document.createElement("li");
							link.innerHTML = "<a href='" + data.results[i].collectionViewUrl + "''><img src='assets/play.svg' width='50' height='50'></a>";
							link.setAttribute("href", data.results[i].collectionViewUrl);
							link.setAttribute("class", "albumLink");
							ul.appendChild(link);

							var addFav = document.createElement("li");
							addFav.innerHTML= "<a href=''>Add to favorites</a>"
							addFav.setAttribute("class", "addToFav");
							addFav.addEventListener("click", addToFav);
							div.appendChild(ul);
							div.appendChild(addFav);

							itunesDiv.appendChild(div);



						}

						// var addToFavLinks = document.querySelectorAll(".addToFav");

						// console.log(addToFavLinks);

						// [].forEach.call(addToFavLinks, function(e){

						// 		e.removeEventListener("click");
						// 		e.addEventListener("click", addToFav);

						// });

					}else{

							var h2 = document.createElement("h2");
							h2.innerHTML = "No results found...";

							console.log

							itunesDiv.appendChild(h2);


					}

					$(".resultsSection").append(itunesDiv);

			}
		})

		//https://api.deezer.com/version/service/id/method/?parameters
		


	$(".resultsSection").hide().fadeIn();

	};

	// $(".resultsSection").fadeIn(200);

	

}

function deleteFav(e){

	e.preventDefault();

	for (var key in localStorage) {

		var dataArray = localStorage[key].split(";;");

		if (dataArray[3] == e.currentTarget.parentNode.getAttribute("id")) {
			localStorage.removeItem(key);
		};

	}	

	checkFavorites();

}

function addToFav(e){

	e.preventDefault();

	var favName = e.currentTarget.parentNode.querySelector(".albumName").innerHTML;
	var favLink = e.currentTarget.parentNode.querySelector(".albumLink").getAttribute("href");
	var favImg = e.currentTarget.parentNode.querySelector(".albumImage").getAttribute("src");
	var favID = window.localStorage.length;

	window.localStorage.setItem(favName, favName + ";;" + favImg + ";;" + favLink + ";;" + "fav" + favID);

	checkFavorites();

	// checkFavorites();

}